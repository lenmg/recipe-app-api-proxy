# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to liste on (default: 8000)
* `APP_HOST` - HOstname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app forward requests to (default: `9000`)
